package multiple_instances;


import com.jogamp.newt.Display;
import com.jogamp.newt.event.*;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.GLBuffers;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import static com.jogamp.opengl.GL.GL_UNSIGNED_SHORT;
import static com.jogamp.opengl.GL2ES2.GL_DEBUG_SEVERITY_HIGH;
import static com.jogamp.opengl.GL2ES2.GL_DEBUG_SEVERITY_MEDIUM;
import static com.jogamp.opengl.GL2ES3.*;
import static com.jogamp.opengl.GL2ES3.GL_UNIFORM_BUFFER;
import static com.jogamp.opengl.GL4.GL_MAP_COHERENT_BIT;
import static com.jogamp.opengl.GL4.GL_MAP_PERSISTENT_BIT;

//DETTE FUNKER!
//Dette funker bra!

/*
* This class is loosly based on the code found at the following link
* https://github.com/java-opengl-labs/hello-triangle/blob/master/src/main/java/gl4/HelloTriangleSimple.java
* 
* The class creates a window capable of rendering graphics using OpenGL 4
*/
public class MultipleInstances implements GLEventListener, KeyListener {

    // OpenGL window reference
    private static GLWindow window;

    // The animator is responsible for continuous operation
    private static Animator animator;

    // The program entry point
    public static void main(String[] args) {
        new MultipleInstances().setup();
    }

    // Vertex data
    private float[] vertexData = {
        // Front
        -1.0f, 1.0f, 1.0f, 0.0f, 2.0f,
        -1.0f, -1.0f, 1.0f, 0.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 2.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 2.0f, 2.0f,
        // Back
        1.0f, 1.0f, -1.0f, 0.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
        // Left
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        // Right
        1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 0.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
        // Top
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
        // Bottom
        -1.0f, -1.0f, 1.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,  1.0f, 1.0f
    };

    // Triangles
    private short[] elementData = {
        // Front
        0, 1, 2, 2, 3, 0,
        // Back
        4, 5, 6, 6, 7, 4,
        // Left
        8, 9, 10, 10, 11, 8,
        // Right
        12, 13, 14, 14, 15, 12,
        // Top
        16, 17, 18, 18, 19, 16,
        // Bottom
        20, 21, 22, 22, 23, 20
    };
    
//NYTT    
    // Camera properties 
    private float[] cameraProperties = {
        0f, 0f, 2f
    };
//
    
    // Interface for creating final static variables for defining the buffers
    private interface Buffer {
        int VERTEX = 0;
        int ELEMENT = 1;

        int GLOBAL_MATRICES = 3;
        int MODEL_MATRIX1 = 4;
        int MODEL_MATRIX2 = 5;
        int MODEL_MATRIX3 = 6;
//NTTT
        int MODEL_MATRIX4 = 7;
        int MODEL_MATRIX5 = 8;
        int MODEL_MATRIX6 = 9;
        int MODEL_MATRIX7 = 10;
        int MODEL_MATRIX8 = 11;
        int MODEL_MATRIX9 = 12;
        int MODEL_MATRIX10 = 13;
//
        int MAX = 14;
    }

    // The OpenGL profile
    GLProfile glProfile;

//NYTT INT SOM ANGIR ANTALL GANGER L�KKEN LENGRE NED SKAL G� GJENNOM. 8 = 8 TEKSTURER. PR�V GJERNE � KOMMENTER UT "gul", sett noOfTextures til 9 og
    //kommenter ut: gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(8)); lengre ned for � pr�ve � kj�re 9 teksturer. jeg fikk bare error.
    private int noOfTextures = 8;
//    

    
//KLARER BARE 8 FORSKJELLIGE TEKSTURER AV GANGEN UTEN ERROR.    
    private final String[] texture = {"texture", "turkis", "ansikt", "Solid_blue", "game", "purple", "rundtHode", "green"};//, "gul", "noe"};

    // Create buffers for the names
    private IntBuffer bufferNames = GLBuffers.newDirectIntBuffer(Buffer.MAX);
    private IntBuffer vertexArrayName = GLBuffers.newDirectIntBuffer(1);
    private IntBuffer textureNames = GLBuffers.newDirectIntBuffer(noOfTextures);

    
    // Create buffers for clear values
    private FloatBuffer clearColor = GLBuffers.newDirectFloatBuffer(new float[] {0, 0, 0, 0});
    private FloatBuffer clearDepth = GLBuffers.newDirectFloatBuffer(new float[] {1});

//NYTT FOR � LEGGE TIL FLERE MATRISER    
    // Create references to buffers for holding the matrices
    private ByteBuffer globalMatricesPointer, modelMatrixPointer1, modelMatrixPointer2, modelMatrixPointer3, modelMatrixPointer4,
    					modelMatrixPointer5, modelMatrixPointer6, modelMatrixPointer7, modelMatrixPointer8, modelMatrixPointer9, modelMatrixPointer10;
//
    
    // https://jogamp.org/bugzilla/show_bug.cgi?id=1287
    private boolean bug1287 = true;

    // Program instance reference
    private Program program;

    // Variable for storing the start time of the application
    private long start;

    
    // Application setup function
    private void setup() {

        // Get a OpenGL 4.x profile (x >= 0)
        glProfile = GLProfile.get(GLProfile.GL4);

        // Get a structure for definining the OpenGL capabilities with default values
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);

        // Create the window with default capabilities
        window = GLWindow.create(glCapabilities);

        // Set the title of the window
        window.setTitle("Kult");

        // Set the size of the window
        window.setSize(1024, 768);

        // Set debug context (must be set before the window is set to visible)
        window.setContextCreationFlags(GLContext.CTX_OPTION_DEBUG);

        // Make the window visible
        window.setVisible(true);

        // Add OpenGL and keyboard event listeners
        window.addGLEventListener(this);
        window.addKeyListener(this);

        // Create and start the animator
        animator = new Animator(window);
        animator.start();

        // Add window event listener
        window.addWindowListener(new WindowAdapter() {
            // Window has been destroyed
            @Override
            public void windowDestroyed(WindowEvent e) {
                // Stop animator and exit
                animator.stop();
                System.exit(1);
            }
        });
    }

    // GLEventListener.init implementation
    @Override
    public void init(GLAutoDrawable drawable) {

        // Get OpenGL 4 reference
        GL4 gl = drawable.getGL().getGL4();

        // Initialize debugging
        initDebug(gl);

        // Initialize buffers
        initBuffers(gl);

        // Initialize vertex array
        initVertexArray(gl);

        // Initialize texture
        initTexture(gl);
             
        // Set up the program
        program = new Program(gl, "multiple_instances", "multiple_instances", "multiple_instances");

        // Enable Opengl depth buffer testing
        gl.glEnable(GL_DEPTH_TEST);

        // Store the starting time of the application
        start = System.currentTimeMillis();

    }
    
    // GLEventListener.display implementation
    public void display(GLAutoDrawable drawable) {

        // Get OpenGL 4 reference
        GL4 gl = drawable.getGL().getGL4();

        
        // Copy the view matrix to the server
        {
            
         // Create identity matrix
            float[] view = FloatUtil.makeTranslation(new float[16], 0, false, 
            				-cameraProperties[0], -cameraProperties[1], -cameraProperties[2]);
            
            // Copy each of the values to the second of the two global matrices
            for (int i = 0; i < 16; i++)
                globalMatricesPointer.putFloat(16 * 4 + i * 4, view[i]); 

        }


        // Clear the color and depth buffers
        gl.glClearBufferfv(GL_COLOR, 0, clearColor);
        gl.glClearBufferfv(GL_DEPTH, 0, clearDepth);

        // Activate the vertex program and vertex array
        gl.glUseProgram(program.name);
        gl.glBindVertexArray(vertexArrayName.get(0));
//NYTT       
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(0));
//        
        

        // Bind the global matrices buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM0,
                bufferNames.get(Buffer.GLOBAL_MATRICES));

        // Copy the model matrix to the server
        {
            // Find a time delta for the time passed since the start of execution
            long now = System.currentTimeMillis();
            float diff = (float) (now - start) / 1_000;       
            
            
//NYTT Scalering og posisjonering av objektene          
            // Create a scale matrix 
            float[] scale = FloatUtil.makeScale(new float[16], true, 0.15f, 0.15f, 0.15f);
            float[] scale2 = FloatUtil.makeScale(new float[16], true, 0.05f, 0.1f, 0.06f);
            float[] skuldre = FloatUtil.makeScale(new float[16], true, 0.18f, 0.04f, 0.04f);
            float[] ben = FloatUtil.makeScale(new float[16], true, 0.06f, 0.06f, 0.1f);


            // Create translation matrixes 
            float[] translate1 = FloatUtil.makeTranslation(new float[16], 0, true, 0f, 3f, 0f); //HODE
            float[] translate2 = FloatUtil.makeTranslation(new float[16], 0, true, 2f, 0.4f, 0f);//RUNDT HODE
            float[] translate7 = FloatUtil.makeTranslation(new float[16], 0, true, 2f, 0.4f, 0f);//RUNDT HODE2            
            float[] translate3 = FloatUtil.makeTranslation(new float[16], 0, true, 0f, -0.2f, 11f); //KROPP            
            float[] translate4 = FloatUtil.makeTranslation(new float[16], 0, true, -1.8f, 2.6f, 6f); //SKULDER VENSTRE
            float[] translate9 = FloatUtil.makeTranslation(new float[16], 0, true, 1.8f, 2.6f, 6f); //H�YRE SKULDER
            float[] translate5 = FloatUtil.makeTranslation(new float[16], 0, true, -2f, -8f, 1f); //BEN VENSTRE
            float[] translate6 = FloatUtil.makeTranslation(new float[16], 0, true, 2f, -8f, 1f);// BEN H�YRE           
            float[] translate8 = FloatUtil.makeTranslation(new float[16], 0, true, 0f, 1f, 1f);//RUNDT VENSTRE ARM           
            float[] translate10 = FloatUtil.makeTranslation(new float[16], 0, true, 0f, 1f, 1f);//RUNDT H�YRE ARM
         

            // Creating rotation matrixes
            float[] rotate = FloatUtil.makeRotationAxis(new float[16], 0, 0, 0f, 1f, 0f, new float[3]);
            float[] rotate2 = FloatUtil.makeRotationAxis(new float[16], 0, -diff*2, 0f, 1f, 0f, new float[3]);            
            float[] rotate3 = FloatUtil.makeRotationAxis(new float[16], 0, diff, 0f, 1f, 0f, new float[3]);
            float[] rotate4 = FloatUtil.makeRotationAxis(new float[16], 0, diff*3, 1f, 0f, 0f, new float[3]);
            float[] rotate7 = FloatUtil.makeRotationAxis(new float[16], 0, diff*3, -1f, 0f, 0f, new float[3]);
            float[] rotate10 = FloatUtil.makeRotationAxis(new float[16], 0, diff, 0f, 1f, 0f, new float[3]); //HODE ROT
            float[] rotate11 = FloatUtil.makeRotationAxis(new float[16], 0, diff, 0f, 1f, 0f, new float[3]); //KROPP ROT
            float[] rotate12 = FloatUtil.makeRotationAxis(new float[16], 0, 0, 0f, 1f, 0f, new float[3]); //VENSTRE BEN ROT
            float[] rotate13 = FloatUtil.makeRotationAxis(new float[16], 0, 0, 0f, 1f, 0f, new float[3]); // H�YRE BEN ROT
            float[] rotate14 = FloatUtil.makeRotationAxis(new float[16], 0, 0, 0f, 1f, 0f, new float[3]); //VENSTRE ARM ROT
            float[] rotate15 = FloatUtil.makeRotationAxis(new float[16], 0, 0, 0f, 1f, 0f, new float[3]); //H�YRE ARM ROT
//
            
            // Combine the ten matrices by multiplying them
// ROTASJON RUNDT HODE            
            float[] model1 = FloatUtil.multMatrix(FloatUtil.multMatrix(scale, translate1, new float[16]), rotate10, new float[16]); //HODE
            float[] modelPos2 = FloatUtil.multMatrix(model1, FloatUtil.multMatrix(rotate3, translate2, new float[16]), new float[16]);
            
            float[] model2 = FloatUtil.multMatrix(modelPos2, FloatUtil.multMatrix(scale, translate2, new float[16]), rotate);//RUNDT HODE
//            
            
// ROTASJON RUNDT HODE           
            
            float[] modelPos3 = FloatUtil.multMatrix(model1, FloatUtil.multMatrix(rotate2, translate2, new float[16]), new float[16]);
            float[] model7 = FloatUtil.multMatrix(modelPos3, FloatUtil.multMatrix(scale, translate7, new float[16]), rotate2);//RUNDT HODE2
                        
            float[] model3 = FloatUtil.multMatrix(FloatUtil.multMatrix(scale2, translate3, new float[16]), rotate11, new float[16]);
//            

//BENA            
            float[] model5 = FloatUtil.multMatrix(FloatUtil.multMatrix(ben, translate5, new float[16]), rotate12, new float[16]);         //BEN VENSTRE   
            float[] model6 = FloatUtil.multMatrix(FloatUtil.multMatrix(ben, translate6, new float[16]), rotate13, new float[16]);         //BEN H�YRE   
//   
            
//ROTASJON RUNDT ARM            
            float[] model4 = FloatUtil.multMatrix(FloatUtil.multMatrix(skuldre, translate4, new float[16]), rotate14, new float[16]); //VENSTRE ARM
            float[] modelPos4 = FloatUtil.multMatrix(model4, FloatUtil.multMatrix(rotate4, translate8, new float[16]), new float[16]);         
            
            float[] model8 = FloatUtil.multMatrix(modelPos4, FloatUtil.multMatrix(scale, translate8, new float[16]), rotate4); 	//RUNDT VENSTRE ARM  
            
//ROTASJON RUNDT ARM            
            float[] model9 = FloatUtil.multMatrix(FloatUtil.multMatrix(skuldre, translate9, new float[16]), rotate15, new float[16]);
            float[] modelPos5 = FloatUtil.multMatrix(model9, FloatUtil.multMatrix(rotate7, translate10, new float[16]), new float[16]);  
            
            float[] model10 = FloatUtil.multMatrix(modelPos5, FloatUtil.multMatrix(scale, translate10, new float[16]), rotate7);   //RUNDT H�YRE ARM
            
            
            
            // Copy the entire matrix to the server
            modelMatrixPointer1.asFloatBuffer().put(model1);
            modelMatrixPointer2.asFloatBuffer().put(model2);
            modelMatrixPointer3.asFloatBuffer().put(model3);
//NYTT            
            modelMatrixPointer4.asFloatBuffer().put(model4);
            modelMatrixPointer5.asFloatBuffer().put(model5);
            modelMatrixPointer6.asFloatBuffer().put(model6);
            modelMatrixPointer7.asFloatBuffer().put(model7);
            modelMatrixPointer8.asFloatBuffer().put(model8);
            modelMatrixPointer9.asFloatBuffer().put(model9);
            modelMatrixPointer10.asFloatBuffer().put(model10);
//
            
        }
        
        // Activate the vertex program and vertex array
        gl.glUseProgram(program.name);
        gl.glBindVertexArray(vertexArrayName.get(0));
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(0));

        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX1));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);

        //NYTT NEDOVER HELER MED gl.glBindTexture og � binde forskjellige texturer p� forskjellige modeller.
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(1));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX2));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(2));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX3));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
//NYTT        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(3));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX4));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(4));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX5));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(5));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX6));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(6));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX7));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(7));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX8));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        //gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(8));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX9));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
        
        //gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(9));
        // Bind the model matrix buffer to a specified index within the uniform buffers
        gl.glBindBufferBase(
                GL_UNIFORM_BUFFER,
                Semantic.Uniform.TRANSFORM1,
                bufferNames.get(Buffer.MODEL_MATRIX10));

        // Draw the triangle
        gl.glDrawElements(
                GL_TRIANGLES,
                elementData.length,
                GL_UNSIGNED_SHORT,
                0);
//       

        // Deactivate the program and vertex array
        gl.glUseProgram(0);
        gl.glBindVertexArray(0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, 0);
    }

    
    // GLEventListener.reshape implementation
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        // Get OpenGL 4 reference
        GL4 gl = drawable.getGL().getGL4();

        // Create an orthogonal projection matrix 
        float[] ortho = FloatUtil.makePerspective(new float[16], 0, false, (float)Math.PI/2f, (float)width/height, 0.1f, 100f);
		
        // Copy the projection matrix to the server
        globalMatricesPointer.asFloatBuffer().put(ortho);

        // Set the OpenGL viewport
        gl.glViewport(x, y, width, height);
    }

    // GLEventListener.dispose implementation
    @Override
    public void dispose(GLAutoDrawable drawable) {

        // Get OpenGL 4 reference
        GL4 gl = drawable.getGL().getGL4();

        // Unmap the transformation matrices
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.GLOBAL_MATRICES));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX1));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX2));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX3));
//NYTT
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX4));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX5));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX6));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX7));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX8));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX9));
        gl.glUnmapNamedBuffer(bufferNames.get(Buffer.MODEL_MATRIX10));
//        

        // Delete the program
        gl.glDeleteProgram(program.name);

        // Delete the vertex array
        gl.glDeleteVertexArrays(1, vertexArrayName);

        // Delete the buffers
        gl.glDeleteBuffers(Buffer.MAX, bufferNames);

        gl.glDeleteTextures(noOfTextures, textureNames);
    }
    
//NYTT LA TIL KEY LISTENER FOR � ZOOME INN OG UT.    
    // KeyListener.keyPressed implementation
    @Override
    public void keyPressed(KeyEvent e) {
    	// Zoom in
    	if (e.getKeyCode() == KeyEvent.VK_UP)
    		cameraProperties[2] -= 0.1f;
    	// Zoom out
    	if (e.getKeyCode() == KeyEvent.VK_DOWN)
    		cameraProperties[2] += 0.1f;
        // Destroy the window if the esape key is pressed
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            new Thread(() -> {
                window.destroy();
            }).start();
        }
    }
//    

    // KeyListener.keyPressed implementation
    @Override
    public void keyReleased(KeyEvent e) {
    }

    // Function for initializing OpenGL debugging
    private void initDebug(GL4 gl) {

        // Register a new debug listener
        window.getContext().addGLDebugListener(new GLDebugListener() {
            // Output any messages to standard out
            @Override
            public void messageSent(GLDebugMessage event) {
                System.out.println(event);
            }
        });

        // Ignore all messages
        gl.glDebugMessageControl(
                GL_DONT_CARE,
                GL_DONT_CARE,
                GL_DONT_CARE,
                0,
                null,
                false);

        // Enable messages of high severity
        gl.glDebugMessageControl(
                GL_DONT_CARE,
                GL_DONT_CARE,
                GL_DEBUG_SEVERITY_HIGH,
                0,
                null,
                true);

        // Enable messages of medium severity
        gl.glDebugMessageControl(
                GL_DONT_CARE,
                GL_DONT_CARE,
                GL_DEBUG_SEVERITY_MEDIUM,
                0,
                null,
                true);
    }

    // Function fo initializing OpenGL buffers
    private void initBuffers(GL4 gl) {

        // Create a new float direct buffer for the vertex data 
        FloatBuffer vertexBuffer = GLBuffers.newDirectFloatBuffer(vertexData);

        // Create a new short direct buffer for the triangle indices
        ShortBuffer elementBuffer = GLBuffers.newDirectShortBuffer(elementData);

        // Create the OpenGL buffers
        gl.glCreateBuffers(Buffer.MAX, bufferNames);

        // If the workaround for bug 1287 isn't needed
        if (!bug1287) {

            // Create and initialize a named buffer storage for the vertex data
            gl.glNamedBufferStorage(bufferNames.get(Buffer.VERTEX), vertexBuffer.capacity() * Float.BYTES, vertexBuffer, GL_STATIC_DRAW);

            // Create and initialize a named buffer storage for the triangle indices
            gl.glNamedBufferStorage(bufferNames.get(Buffer.ELEMENT), elementBuffer.capacity() * Short.BYTES, elementBuffer, GL_STATIC_DRAW);

            // Create and initialize a named buffer storage for the global and model matrices 
            gl.glNamedBufferStorage(bufferNames.get(Buffer.GLOBAL_MATRICES), 16 * 4 * 2, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX1), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX2), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX3), 16 * 4, null, GL_MAP_WRITE_BIT);            
//NYTT		
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX4), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX5), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX6), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX7), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX8), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX9), 16 * 4, null, GL_MAP_WRITE_BIT);
            gl.glNamedBufferStorage(bufferNames.get(Buffer.MODEL_MATRIX10), 16 * 4, null, GL_MAP_WRITE_BIT);
            
//

        } else {

            // Create and initialize a buffer storage for the vertex data
            gl.glBindBuffer(GL_ARRAY_BUFFER, bufferNames.get(Buffer.VERTEX));
            gl.glBufferStorage(GL_ARRAY_BUFFER, vertexBuffer.capacity() * Float.BYTES, vertexBuffer, 0);
            gl.glBindBuffer(GL_ARRAY_BUFFER, 0);

            // Create and initialize a buffer storage for the triangle indices 
            gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferNames.get(Buffer.ELEMENT));
            gl.glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, elementBuffer.capacity() * Short.BYTES, elementBuffer, 0);
            gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


            // Retrieve the uniform buffer offset alignment minimum
            IntBuffer uniformBufferOffset = GLBuffers.newDirectIntBuffer(1);
            gl.glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, uniformBufferOffset);
            // Set the required bytes for the matrices in accordance to the uniform buffer offset alignment minimum
            int globalBlockSize = Math.max(16 * 4 * 2, uniformBufferOffset.get(0));
            int modelBlockSize = Math.max(16 * 4, uniformBufferOffset.get(0));

            
            // Create and initialize a named storage for the global matrices 
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.GLOBAL_MATRICES));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, globalBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);

            // Create and initialize a named storage for the model matrices 
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX1));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX2));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX3));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            
//NYTT
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX4));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX5));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX6));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX7));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX8));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX9));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, bufferNames.get(Buffer.MODEL_MATRIX10));
            gl.glBufferStorage(GL_UNIFORM_BUFFER, modelBlockSize, null, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
            gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
            
//            
            
        }

        // map the global matrices buffer into the client space
        globalMatricesPointer = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.GLOBAL_MATRICES),
                0,
                16 * 4 * 2,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT); // flags

        // map the model matrix buffer into the client space
        modelMatrixPointer1 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX1),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer2 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX2),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer3 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX3),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        
//NYTT        
        modelMatrixPointer4 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX4),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer5 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX5),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer6 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX6),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer7 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX7),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer8 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX8),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer9 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX9),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        modelMatrixPointer10 = gl.glMapNamedBufferRange(
                bufferNames.get(Buffer.MODEL_MATRIX10),
                0,
                16 * 4,
                GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
//        
    }

    // Function for initializing the vertex array
    private void initVertexArray(GL4 gl) {

        // Create a single vertex array object
        gl.glCreateVertexArrays(1, vertexArrayName);

        // Associate the vertex attributes in the vertex array object with the vertex buffer
        gl.glVertexArrayAttribBinding(vertexArrayName.get(0), Semantic.Attr.POSITION, Semantic.Stream.A);
        gl.glVertexArrayAttribBinding(vertexArrayName.get(0), Semantic.Attr.TEXCOORD, Semantic.Stream.A);
    
        // Set the format of the vertex attributes in the vertex array object
        gl.glVertexArrayAttribFormat(vertexArrayName.get(0), Semantic.Attr.POSITION, 3, GL_FLOAT, false, 0);
        gl.glVertexArrayAttribFormat(vertexArrayName.get(0), Semantic.Attr.TEXCOORD, 2, GL_FLOAT, false, 3 * 4);

        // Enable the vertex attributes in the vertex object
        gl.glEnableVertexArrayAttrib(vertexArrayName.get(0), Semantic.Attr.POSITION);
        gl.glEnableVertexArrayAttrib(vertexArrayName.get(0), Semantic.Attr.TEXCOORD);


        // Bind the triangle indices in the vertex array object the triangle indices buffer
        gl.glVertexArrayElementBuffer(vertexArrayName.get(0), bufferNames.get(Buffer.ELEMENT));

        // Bind the vertex array object to the vertex buffer
        gl.glVertexArrayVertexBuffer(vertexArrayName.get(0), Semantic.Stream.A, bufferNames.get(Buffer.VERTEX), 0, (2 + 3) * 4);
    }

    private void initTexture(GL4 gl) {
        try {
//NYTT EN LA INN EN L�KKE FOR � HENTE INN FLERE TEKSTURER        	
        	  // Generate texture name
            gl.glGenTextures(noOfTextures, textureNames);

            for (int i=0; i<noOfTextures; i++) {

				TextureData textureData = TextureIO.newTextureData(glProfile, new File( "src/multiple_instances/" + texture[i] + ".png"), false, TextureIO.PNG);

               gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(i));

             // Specify the format of the texture
            gl.glTexImage2D(gl.GL_TEXTURE_2D,  0, 
                textureData.getInternalFormat(), 
                textureData.getWidth(), textureData.getHeight(), 
                textureData.getBorder(),
                textureData.getPixelFormat(), 
                textureData.getPixelType(),
                textureData.getBuffer());
            System.out.println("glTexImage2D " + gl.glGetError());

            // Set the sampler parameters
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            // Generate mip maps
            gl.glGenerateMipmap(GL_TEXTURE_2D);

            // Deactivate texture
            gl.glBindTexture(GL_TEXTURE_2D, textureNames.get(i));
            // Bind the texture
           // gl.glBindTexture(gl.GL_TEXTURE_2D, textureNames.get(i));
            
            }
      
//            
        }
        catch (IOException io) {
            io.printStackTrace();
        }
    }
    

    // Private class representing a vertex program
    private class Program {

        // The name of the program
        public int name = 0;

        // Constructor
        public Program(GL4 gl, String root, String vertex, String fragment) {

            // Instantiate a complete vertex shader
            ShaderCode vertShader = ShaderCode.create(gl, GL_VERTEX_SHADER, this.getClass(), root, null, vertex,
                    "vert", null, true);

            // Instantiate a complete fragment shader
            ShaderCode fragShader = ShaderCode.create(gl, GL_FRAGMENT_SHADER, this.getClass(), root, null, fragment,
                    "frag", null, true);

            // Create the shader program
            ShaderProgram shaderProgram = new ShaderProgram();

            // Add the vertex and fragment shader
            shaderProgram.add(vertShader);
            shaderProgram.add(fragShader);

            // Initialize the program
            shaderProgram.init(gl);

            // Store the program name (nonzero if valid)
            name = shaderProgram.program();

            // Compile and link the program
            shaderProgram.link(gl, System.out);
        }
    }

    // Private class to provide an semantic interface between Java and GLSL
	private static class Semantic {

		public interface Attr {
			int POSITION = 0;
			int TEXCOORD = 1;
		}

		public interface Uniform {
			int TRANSFORM0 = 1;
			int TRANSFORM1 = 2;
		}

		public interface Stream {
			int A = 0;
		}
	}
}
